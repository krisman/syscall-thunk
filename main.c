#include <stdio.h>
#include <signal.h>
#include <linux/seccomp.h>
#include <linux/filter.h>
#include <linux/audit.h>
#include <sys/ptrace.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <err.h>
#include <string.h>
#include <sys/mman.h>

//typedef unsigned long (*syscall_fn_t)(long number,
//				      unsigned long arg1, unsigned long arg2,
//				      unsigned long arg3, unsigned long arg4,
//				      unsigned long arg5, unsigned long arg6);

extern void* (entry_point) (void);
extern void* (syscall_instruction)(void);

extern unsigned int raw_syscall(long number,
				  unsigned long arg1, unsigned long arg2,
				  unsigned long arg3, unsigned long arg4,
				  unsigned long arg5, unsigned long arg6);




unsigned int log_syscall(int num, unsigned long arg1, unsigned long arg2,
			 unsigned long arg3, unsigned long arg4, unsigned long arg5,
			 unsigned long arg6)
{
	char buf[1024];
	int r;

	r = snprintf(buf, 1024,
		     "syscall:%d(%lu, %lu, %lu, %lu, %lu, %lu)\n",
		     num, arg1, arg2, arg3, arg4, arg5, arg6);

	raw_syscall(SYS_write, 1, (unsigned long)buf, r, 0, 0, 0);

	return raw_syscall(num, arg1, arg2, arg3, arg4,arg5,arg6);
}

int main (void)
{
	int r;

	printf("setting entry_point=%llx dispatcher=%llx\n", entry_point, syscall_instruction);

	r = prctl(59, entry_point, syscall_instruction, 0 ,0, 0);
	if (r)
		err (1, "bug");

	printf("hello world\n");




	return 0;
}
